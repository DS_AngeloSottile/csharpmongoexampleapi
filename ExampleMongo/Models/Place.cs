﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ExampleMongo.Models;

public class Place
{
    
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string? Id { get; set; }

    public string Name { get; set; } = null!;
}