﻿using ExampleMongo.Models;
using ExampleMongo.Services;
using Microsoft.AspNetCore.Mvc;

namespace ExampleMongo.Controllers;

[ApiController]
[Route("api/[controller]")]
public class BooksController : ControllerBase
{
    private readonly IBooksService _booksService;

    public BooksController(IBooksService booksService) => _booksService = booksService;

    [HttpGet]
    [Route("findAll")]
    public async Task<List<Book>> FindAll()
    {
        return await _booksService.GetAsync();
    }  

    [HttpGet]
    [Route("FindById/{id:length(24)}")]
    public async Task<ActionResult<Book>> FindById(string id)
    {
        var book = await _booksService.GetAsync(id);

        if (book is null)
        {
            return NotFound();
        }

        return book;
    }

    [HttpPost]
    [Route("CreateBook")]
    public async Task<IActionResult> CreateBook(Book newBook)
    {
        await _booksService.CreateAsync(newBook);

        return CreatedAtAction(nameof(FindById), new { id = newBook.Id }, newBook);
    }

    [HttpPut]
    [Route("UpdateBookById/{id:length(24)}")]
    public async Task<IActionResult> UpdateBookById(string id, Book updatedBook)
    {
        var book = await _booksService.GetAsync(id);

        if (book is null)
        {
            return NotFound();
        }

        updatedBook.Id = book.Id;

        await _booksService.UpdateAsync(id, updatedBook);

        return NoContent();
    }

    [HttpDelete("DeleteBookById/{id:length(24)}")]
    public async Task<IActionResult> DeleteBookById(string id)
    {
        var book = await _booksService.GetAsync(id);

        if (book is null)
        {
            return NotFound();
        }

        await _booksService.RemoveAsync(id);

        return NoContent();
    }
}