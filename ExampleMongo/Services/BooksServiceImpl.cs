﻿using ExampleMongo.Infrastructure;
using ExampleMongo.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace ExampleMongo.Services;

public class BooksServiceImpl : IBooksService
{
    private readonly IMongoCollection<Book> _booksCollection;

    public BooksServiceImpl(IOptions<BookStoreDatabaseSettings> bookStoreDatabaseSettings)
    {
        var mongoClient = new MongoClient(bookStoreDatabaseSettings.Value.ConnectionString);

        var mongoDatabase = mongoClient.GetDatabase(bookStoreDatabaseSettings.Value.DatabaseName);

        _booksCollection = mongoDatabase.GetCollection<Book>(bookStoreDatabaseSettings.Value.BooksCollectionName);
    }

    public async Task<List<Book>> GetAsync()
    { 
        return await  _booksCollection.Find(_ => true).ToListAsync();
    }

    public async Task<Book?> GetAsync(string id) =>
        await _booksCollection.Find(x => x.Id == id).FirstOrDefaultAsync();

    public async Task CreateAsync(Book newBook)
    { 
        await _booksCollection.InsertOneAsync(newBook);
    }

    public async Task UpdateAsync(string id, Book updatedBook)
    {
        await _booksCollection.ReplaceOneAsync(book => book.Id == id, updatedBook);
    }

    public async Task RemoveAsync(string id) =>
        await _booksCollection.DeleteOneAsync(x => x.Id == id);
}